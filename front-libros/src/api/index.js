// implement your own methods in here, if your data is coming from A rest API

import * as User from './user';
export default {
  // user
  getUser: User.getUser,
  getUserById: User.getUserById
};