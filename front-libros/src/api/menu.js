const Menu =  [
  {
    title: 'MENU PRINCIPAL',
    group: 'MENU PRINCIPAL',
    component: 'layout',
    icon: 'view_compact',
  },
  { header: "LIBROS"},
  {
    title: "Libros",
    group: "Libros",
    component: "Libros",
    icon: "tune",
    items: [
      { name: "Libro", title: "LIBRO" ,titlePage: "GESTION DE LIBROS", badge: "new", component: "views/libro" },
    ]
  },
  { divider: true },

  { header: "Config"},
  {
    title: "Parametros",
    group: "Config",
    component: "Config",
    icon: "tune",
    items: [
      { name: "autor", title: "Autor" ,titlePage: "Autor", badge: "new", component: "views/autor" },
      
    ]
    }

];
// reorder menu
Menu.forEach((item) => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
});

export default Menu;
