export default [
  {
    name: 'APP_LOGIN_SUCCESS',
    callback: function (e) {
      this.$router.push({ path: 'principal' });
    }
  },
  {
    name: 'APP_PERFIL',
    callback: function (e) {
      this.$router.push({ path: '/perfil' });
    }
  },
  {
    name: 'APP_NETWORK_FAIL',
    callback: function (e) {
      this.snackbar = {
        show: true,
        color: 'red',
        text: 'No se pudo iniciar sesiòn, servidor no encontrado.'
      };
      this.$router.push({ path: '/login' });
    }
  },
  {
    name: 'APP_LOGOUT',
    callback: function (e) {
      this.snackbar = {
        show: true,
        color: 'green',
        text: 'La sesion se cerró.'
      };
      localStorage.clear();
      this.$router.replace({ path: '/login' });
    }
  },
  {
    name: 'APP_PAGE_LOADED',
    callback: function (e) {
    }
  },
  {
    name: 'APP_AUTH_FAILED',
    callback: function (e) {
      this.$router.push('/login');
      this.$message.error('el token ha expirado');
    }
  },
  {
    name: 'APP_BAD_REQUEST',
    // @error api response data
    callback: function (msg) {
      this.$message.error(msg);
    }
  },
  {
    name: 'APP_ACCESS_DENIED',
    // @error api response data
    callback: function (msg) {
      this.$message.error(msg);
      this.$router.push('/forbidden');
    }
  },
  {
    name: 'APP_RESOURCE_DELETED',
    // @error api response data
    callback: function (msg) {
      this.$message.success(msg);
    }
  },
  {
    name: 'APP_RESOURCE_UPDATED',
    // @error api response data
    callback: function (msg) {
      this.$message.success(msg);
    }
  },
  {
    name: 'APP_LOADING',
    callback: function (a=false,b='') {
      if (this.boxLoad.show!=a) this.boxLoad.show = a;
      if (b!='') this.boxLoad.text = b;
      setTimeout(()=>'', 3000);
     }
  },
];
