import Vue           from 'vue';
import VTreeview from 'v-treeview';
 
/*
or for SSR:
import Notifications from 'vue-notification/dist/ssr.js'
*/
 
Vue.use(VTreeview);