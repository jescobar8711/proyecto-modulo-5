export default [
  
  { // si ingresa cualquier direccion que no sea la correcta
    path: '*',
    meta: {
      public: true,
    },
    redirect: {
      path: '/404'
    }
  },
  {
    path: '/404',
    meta: {
      public: true,
    },
    name: 'NotFound',
    component: () => import(
      `@/views/NotFound.vue`
    )
  },
  {
    path: '/403',
    meta: {
      public: true,
    },
    name: 'AccessDenied',
    component: () => import(
      `@/views/Deny.vue`
    )
  },
  {
    path: '/500',
    meta: {
      public: true,
    },
    name: 'ServerError',
    component: () => import(
      `@/views/Error.vue`
    )
  },
  {
    path: '/',
    meta: { },
    name: 'root',
    redirect: {
      name: 'login'
    }
  },
  {
    path: '/login',
    meta: {
      public: true,
    },
    name: 'login',
    component: () => import(
      `@/views/Login.vue`
    )
  },
  {
    path: '/principal',
    meta: { breadcrumb: true, autenticado: true },
    name: 'Dashboard',
    component: () => import(
      `@/views/Dashboard.vue`
    )
  },
  {
    path: '/perfil',
    meta: { breadcrumb: true, autenticado: true },
    name: 'perfil',
    component: () => import(
      `@/views/perfil/index.vue`
    )
  },
  /*INICIO UNIDAD DE LIBRO  */
  {
    path: "/views/libro",
    meta: { breadcrumb: true },
    name: "views/libro",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/libro/index.vue`)
  },
 
   
   /*INICIO DE CONFIG */


   {
    path: "/views/autor",
    meta: { breadcrumb: true },
    name: "views/autor",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/autor/index.vue`)
  },
  
];


