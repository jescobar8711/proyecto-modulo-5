import axios from 'axios';

const qs = require('qs');
const apiClient = axios.create({
  baseURL:'https://fast-tundra-59461.herokuapp.com/',
  withCredentials:false,
  headers:{
    Accept:'application/json',
    'Content-Type':'application/json',

    /*jwt: localStorage.token,//elJwt,
        Accept: "application/json",
        "Content-Type": "application/json"*/ 
  }
});

const config = {
  headers: {
    'Accept':'application/json',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    //'Content-Type': 'application/json',
   // 'content-length': 11,
    'Host': 'shrouded-retreat-42788.herokuapp.com',
    'Content-Length': '77',
    'Connection': 'close',
  },
  responseType: 'json'
};
const apiClientSession = axios.create({
  baseURL:'http://shrouded-retreat-42788.herokuapp.com/',
  withCredentials:false,
  headers:{
   'Accept':'application/json',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  }
});
const apiClientFile = axios.create({
  //responseType: "arraybuffer",
  baseURL:'http://shrouded-retreat-42788.herokuapp.com/',
  withCredentials: false,
  //dataType: "json",
  headers: {
   // jwt: localStorage.et, // elJwt,
    Accept: "application/x-www-form-urlencoded",
    'Content-Type': 'application/x-www-form-urlencoded',
   dataType: 'JSON', //wh
  }
});

export default {
  get(url,data =''){
    return apiClient.get(url,data)
  },
  post(url,data){
    return apiClient.post(url,data)
  },
  delete(url,data){
    return apiClient.delete(url,data)
  },
  put(url,data){
    return apiClient.put(url,data)
  },

  setToken(token){
    apiClient.defaults.headers.common['jwt']=token;
  },
  postApiFile(url, data) {
 
return apiClientSession.post(url,data,config);
  // return apiClientSession.post(url,data);
  },

  getApiFile(url, losParams) {
    return apiClientFile.get(url, { params: losParams });
  },
  setJwt(token) {
    localStorage.et = token;
    apiClient.defaults.headers.common['jwt']=token;
  },
  setEmail(email) {
    localStorage.email = email;
    apiClient.defaults.headers.common['email']=email;
  },
  
  getPerfil(){
      if (localStorage.getItem('perfil')) {
    if (JSON.parse(localStorage.getItem('perfil')).length >0) { 
       return JSON.parse(localStorage.getItem('perfil'));
       }else{
        return this.loadPerfil();
       }
      } else {
        return this.loadPerfil();
       }

      },
      async  loadPerfil() {
 
   return await  apiClient.get("perfil/"+ localStorage.getItem('email'), {
        })
        .then(response => {
          if (response.data) {
          
            localStorage.setItem('perfil', JSON.stringify(response.data));
            //this.$store.commit("alerta", { show: true, estado: 200 });
            return response.data;
          } else {
            return response.data;
          }
        })
        .catch(error => {
       //   this.$store.commit("alerta", { show: true, estado: 404 });
        });
  //  });
  
  }
}