export const OpcionesValidar ={
  requerido:v => !!v || 'Campo requerido',
  max10Chars: v => v.length <= 10 || 'el campo debe tener menos de 10 caracteres',
  email:v => {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return pattern.test(v) || 'correo invalido.'
  },
  fecha:v=> (/^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/).test(v)||'Formato de fecha incorrecto',
  numero:v=> (/^(?:(?:(?:0?[1-9]|1\d|2[0-8])[/](?:0?[1-9]|1[0-2])|(?:29|30)[/](?:0?[13-9]|1[0-2])|31[/](?:0?[13578]|1[02]))[/](?:0{2,3}[1-9]|0{1,2}[1-9]\d|0?[1-9]\d{2}|[1-9]\d{3})|29[/]0?2[/](?:\d{1,2}(?:0[48]|[2468][048]|[13579][26])|(?:0?[48]|[13579][26]|[2468][048])00))$/).test(v)||'este campo debe ser numerico'
};
